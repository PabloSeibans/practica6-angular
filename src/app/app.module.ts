import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ContenidoComponent } from './components/contenido/contenido.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { APP_ROUTING } from './app.routes';
import { ListaComponent } from './components/lista/lista.component';
import { ProductosComponent } from './components/productos/productos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    InicioComponent,
    ContenidoComponent,
    ContactoComponent,
    ListaComponent,
    ProductosComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
