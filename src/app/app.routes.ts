import { RouterModule, Routes } from "@angular/router";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { ContenidoComponent } from "./components/contenido/contenido.component";
import { InicioComponent } from "./components/inicio/inicio.component";
import { ListaComponent } from "./components/lista/lista.component";
import { ProductosComponent } from "./components/productos/productos.component";

const APP_ROUTES: Routes = [
    { path: 'inicio', component: InicioComponent },
    { path: 'contenido', component: ContenidoComponent},
    { path: 'lista', component: ListaComponent},
    { path: 'productos', component: ProductosComponent },
    { path: 'contacto', component: ContactoComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
